﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fogContro : MonoBehaviour
{

   public float minFog;
   public float maxFog;
   public float time;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            RenderSettings.fogDensity = Mathf.Lerp(maxFog, minFog, time);
            Debug.Log("FogReduced!");
         
        }
    }
}
