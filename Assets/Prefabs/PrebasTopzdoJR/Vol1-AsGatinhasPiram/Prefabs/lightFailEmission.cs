﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightFailEmission : MonoBehaviour
{
   

    public Light testLight;
    public float minWaitTime;
    public float maxWaitTime;
    public GameObject lamp;


    void Start()
    {
        testLight = GetComponent<Light>();
        StartCoroutine(Flashing());
    }


    IEnumerator Flashing()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
            testLight.enabled = !testLight.enabled;
            if (lamp.activeSelf)
            {
                lamp.SetActive(false);

            }
            else
            {
                lamp.SetActive(true);

            }

        }
    }
}
