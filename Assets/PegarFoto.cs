﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PegarFoto : MonoBehaviour
{
    public GameObject final;

    [SerializeField]
    Sprite action;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void PlayerStay()
    {
        if (CanvasController.instance != null)
        {
            CanvasController.instance.ChangeActionButton(action);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            final.SetActive(true);
            CanvasController.instance.ChangeActionButton(null);
            gameObject.SetActive(false);
        }
    }

    void PlayerExit()
    {
        CanvasController.instance.ChangeActionButton(null);
    }
}
