﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fechar_AbrirPorta : MonoBehaviour
{
    public GameObject portaFechada;
    public GameObject portaAberta;
    public GameObject dialogo;

    // Start is called before the first frame update
    void Start()
    {
        portaAberta.SetActive(false);
        portaFechada.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if(!dialogo.activeInHierarchy)
        {
            portaAberta.SetActive(true);
            portaFechada.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
