﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class intensidade : MonoBehaviour
{
    public GameObject intensidadeLuz;
    public Image cor;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AdjustAmbientIntensity(Slider slider)
    {
        //RenderSettings.ambientIntensity = slider.value;
        RenderSettings.ambientLight = new Color(slider.value, slider.value, slider.value, 1.0f);
        cor.color = new Color(slider.value, slider.value, slider.value, 1.0f);

        VariaveisGlobais.ajustegamma = slider.value;
    }
}
