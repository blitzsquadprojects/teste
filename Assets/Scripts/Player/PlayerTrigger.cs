﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PlayerTrigger : MonoBehaviour
{
    public static string PlayerTag = "Player";

    void Start()
    {
        GetComponent<Collider>().isTrigger = true;
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.CompareTag(PlayerTag))
        {
            SendMessage("PlayerEnter", hit.gameObject, SendMessageOptions.DontRequireReceiver);
        }
    }

    void OnTriggerStay(Collider hit)
    {
        if (hit.CompareTag(PlayerTag))
        {
            SendMessage("PlayerStay", hit.gameObject, SendMessageOptions.DontRequireReceiver);
        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.CompareTag(PlayerTag))
        {
            SendMessage("PlayerExit", hit.gameObject, SendMessageOptions.DontRequireReceiver);
        }
    }
}
