﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{
    Rigidbody rb;

    #region Camera
    Camera cam;
    [SerializeField]
    float MaxX = 0.25f;

    [SerializeField]
    Transform head_Joint;
    #endregion

    Vector3 velocity = Vector3.zero;
    Vector3 rotation = Vector3.zero;
    Vector3 cameraRotation = Vector3.zero;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main;
    }

    public void MovePlayer(Vector3 _velocity)
    {
        velocity = _velocity;
    }

    public void RotatePlayer(Vector3 _rotarion)
    {
        rotation = _rotarion;
    }

    public void RotateCamera(Vector3 _cameraRotation)
    {
        cameraRotation += _cameraRotation;
        cameraRotation.x = Mathf.Clamp(cameraRotation.x, -MaxX, MaxX);
    }

    private void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
        //Repo_Cam();
    }

    //Perform movement based on velocity variable
    void PerformMovement()
    {
        if(velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + velocity * Time.deltaTime);
        }
    }

    void PerformRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        if (cam != null)
        {
            cam.transform.eulerAngles = new Vector3(-cameraRotation.x, cam.transform.eulerAngles.y, 0);
        }
    }

    void Repo_Cam()
    {
        Vector3 h_Pos = head_Joint.position;
        cam.transform.position = new Vector3(cam.transform.position.x, h_Pos.y, h_Pos.z);
    }
}