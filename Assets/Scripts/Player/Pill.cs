﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pill : MonoBehaviour
{
    [SerializeField]
    [Range(0, 10)]
    int value = 1;

    [SerializeField]
    Sprite action;

    static bool hasInstruction = false;
    static string instruction = "Aperte Q para usar a pílula e diminua o nível de radiação";

    void PlayerStay()
    {
        CanvasController.instance.ChangeActionButton(action);
        if (Input.GetKeyDown(KeyCode.E))
        {
            CanvasController.instance.ChangeActionButton(null);
            Player.num_Pill += value;
            gameObject.SetActive(false);

            if (!hasInstruction)
            {
                FeedbacksObjetivos.txtFeedbacksComplementares = instruction;
                FeedbacksObjetivos.feedbackComplementarBool = true;
                hasInstruction = true;
            }
        }
    }

    void PlayerExit()
    {
        CanvasController.instance.ChangeActionButton(null);
    }
}
