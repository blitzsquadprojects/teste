﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public static Player instance { get; private set; }

    [HideInInspector]
    public Move move;
    [HideInInspector]
    public bool canMove = true;

    [Header("Config")]
    [SerializeField]
    float Inital_speed = 30;
    [SerializeField]
    float crouch_speed = 20, run_speed = 40;
    float speed;

    [Space(5)]
    [SerializeField]
    float recharge_Battery;
    [SerializeField]
    float pill_Decrease;

    [Space(5)]
    [SerializeField]
    float lookSensitivity = 3;

    Animator anim;

    [HideInInspector]
    public float radioactive_Receive, current_Radioactive;

    [Header("Radioactive")]
    [SerializeField]
    float decrease_Radioactive;
    [SerializeField]
    public float middle_Recieve;

    [HideInInspector]
    public Flashlight flashlight;
    [HideInInspector]
    public Geiger geiger;

    public static int num_Pill = 0;
    public static int num_Battery = 0;

    void Awake()
    {
        instance = this;
        move = GetComponent<Move>();
        anim = GetComponentInChildren<Animator>();

        if (GetComponentInChildren<Flashlight>() != null)
        {
            flashlight = GetComponentInChildren<Flashlight>();
        }

        if (GetComponentInChildren<Geiger>() != null)
        {
            geiger = GetComponentInChildren<Geiger>();
        }

        current_Radioactive = 100;
        radioactive_Receive = middle_Recieve;
    }

    void Update()
    {
        current_Radioactive -= radioactive_Receive * Time.deltaTime;
        Decrease_Radioactive();
        current_Radioactive = Mathf.Clamp(current_Radioactive, 0, 100);

        if(current_Radioactive <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetKeyDown(KeyCode.Q) && num_Pill > 0)
        {
            current_Radioactive += pill_Decrease;
            num_Pill--;
        }

        if (Input.GetKeyDown(KeyCode.R) && num_Battery > 0)
        {
            flashlight.Recharge(recharge_Battery);
            num_Battery--;
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        if (canMove)
        {
            #region Move
            //Verify speed
            if (Input.GetKey(KeyCode.LeftShift))
            {
                speed = run_speed;
            }
            else if (Input.GetKey(KeyCode.LeftControl))
            {
                speed = crouch_speed;
            }
            else
            {
                speed = Inital_speed;
            }

            //Calculate movement velocity as a 3D vector
            float _xMov = Input.GetAxis("Horizontal");
            float _zMov = Input.GetAxis("Vertical");

            Vector3 _moveHorizontal = transform.right * _xMov;
            Vector3 _moveVertical = transform.forward * _zMov;

            //final movement vector
            Vector3 _velocity = (_moveHorizontal + _moveVertical) * speed;

            //Apply movement
            move.MovePlayer(_velocity);

            //Calculate rotation as a 3D vector(turning around)
            float _yRot = Input.GetAxisRaw("Mouse X");

            Vector3 _rotation = new Vector3(0, _yRot, 0) * lookSensitivity;

            //Aplly rotarion
            move.RotatePlayer(_rotation);

            //Calculate rotation as a 3D vector(turning around)
            float _xRot = Input.GetAxisRaw("Mouse Y");

            Vector3 _cameraRotation = new Vector3(_xRot, 0, 0) * lookSensitivity;

            //Aplly camera rotarion
            move.RotateCamera(_cameraRotation);
            #endregion
        }

        UpdateAnimation();
    }

    void Decrease_Radioactive(){
        if(radioactive_Receive <= 0 && current_Radioactive >= 65){
            current_Radioactive += decrease_Radioactive * Time.deltaTime;
        }
    }

    void UpdateAnimation()
    {
        anim.transform.localPosition = Vector3.zero;
        anim.transform.localEulerAngles = Vector3.zero;
        if (speed == run_speed)
        {
            anim.SetBool("Run", true);
        }
        else if (speed == crouch_speed)
        {
            anim.SetBool("Crouch", true);
        }
        else
        {
            anim.SetBool("Run", false);
            anim.SetBool("Crouch", false);
        }

        anim.SetFloat("Vertical", Input.GetAxis("Vertical"));
        anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
    }
}
