﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerTrigger))]
public class Battery : MonoBehaviour
{
    [SerializeField]
    [Range(0,10)]
    int value = 1;

    [SerializeField]
    Sprite action;

    static bool hasInstruction = false;
    static string instruction = "Aperte R para usar a bateria e aumente a carga da lanterna";

    void PlayerStay()
    {
        CanvasController.instance.ChangeActionButton(action);
        if (Input.GetKeyDown(KeyCode.E))
        {
            CanvasController.instance.ChangeActionButton(null);
            Player.num_Battery += value;
            gameObject.SetActive(false);

            if (!hasInstruction)
            {
                FeedbacksObjetivos.txtFeedbacksComplementares = instruction;
                FeedbacksObjetivos.feedbackComplementarBool = true;
                hasInstruction = true;
            }
        }
    }

    void PlayerExit()
    {
        CanvasController.instance.ChangeActionButton(null);
    }
}
