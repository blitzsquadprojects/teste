﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ApparitionSpace;

[RequireComponent(typeof(Light))]
public class Flashlight : MonoBehaviour
{
    Light lamp_Light;

    float battery;

    [SerializeField]
    float timeDischarge;
    float discharge;

    [SerializeField]
    int maxCharge;

    [Header("Intensy")]
    [SerializeField]
    float timeToMaxIntensy, increseBattery;
    float intensy, currentTimeIntensy;

    bool incresed = false;
    bool increse = true;

    void Start()
    {
        lamp_Light = GetComponent<Light>();
        intensy = lamp_Light.intensity;

        battery = maxCharge;
    }

    void Update()
    {
        battery = Discharge();
        if (battery > 0){
            if (lamp_Light.enabled)
            {
                CanvasController.instance.CheckBatery(battery,maxCharge);

                if (Input.GetMouseButton(0))
                {
                    currentTimeIntensy += (Time.deltaTime / timeToMaxIntensy);
                    if (currentTimeIntensy >= 1)
                    {
                        if (increse)
                        {
                            lamp_Light.intensity *= increseBattery;
                        }
                        else
                        {
                            lamp_Light.intensity /= increseBattery;
                        }

                        currentTimeIntensy = 0;
                        increse = !increse;
                        incresed = true;
                    }
                }
            }
            

            if (Input.GetMouseButtonUp(0))
            {
                if (!incresed)
                {
                    lamp_Light.enabled = !lamp_Light.enabled;
                    lamp_Light.intensity = intensy;
                    currentTimeIntensy = 0;
                }

                incresed = false;
            }
        } 
        else {
            lamp_Light.enabled = false;
        }
        
    }

    float Discharge()
    {
        float multiple_Discharge = (lamp_Light.intensity / intensy);
        discharge += (Time.deltaTime / (timeDischarge / multiple_Discharge)) * maxCharge;
        discharge = Mathf.Clamp(discharge, 0, maxCharge);

        return maxCharge - discharge;
    }

    public void Recharge(float v)
    {
        discharge -= v;
    }
}
