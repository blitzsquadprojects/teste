﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interaction;

public class ObjectiveController : MonoBehaviour
{
    [SerializeField]
    string[] objectives;
    [SerializeField]
    string[] instructions;

    public static string [] objs;
    public static string[] inst;
    static int currentObjective = -1;
    static int currentInstruction = -1;

    /*/CheckPoint
    static ObjectReceiver [] savedObjsReceiver;
    static int savedObjective;
    static Vector3 savedPos;
    public static List<GameObject> resetObjs = new List<GameObject>();*/

    void Awake()
    {
        objs = objectives;
        inst = instructions;
    }

    void Update()
    {
    }

    public static void NextObjective()
    {
        currentObjective++;

        if(currentObjective < objs.Length)
        {
            FeedbacksObjetivos.txtObjetivoAtual = objs[currentObjective];
            FeedbacksObjetivos.FeedbackObjetivoBool = true;
        }
    }

    public static void NextInstruction()
    {
        currentInstruction++;
        if (currentInstruction < inst.Length)
        {
            FeedbacksObjetivos.txtFeedbacksComplementares = inst[currentInstruction];
            FeedbacksObjetivos.feedbackComplementarBool = true;
        }
    }

    /*public static void SaveCheckPoint()
    {
        savedObjective = currentObjective;
        savedObjsReceiver = Inventory.objects;
        savedPos = Player.instance.transform.position;
    }

    public static void GetCheckPoint()
    {
        currentObjective = savedObjective;
        for(int i = 0; i < resetObjs.Count; i++)
        {
            resetObjs[i].SendMessage("ResetCode");
        }
        resetObjs = new List<GameObject>();

        Inventory.ResetInventory();
        Inventory.objects = savedObjsReceiver;
        Player.instance.transform.position = savedPos;
    }*/
}
