﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ApparitionSpace
{
    [RequireComponent(typeof(PlayerTrigger))]
    public class Active : MonoBehaviour
    {
        [SerializeField]
        bool activeImmediately = false, setActive = true;

        void PlayerStay()
        {
            if (Apparition.instance.gameObject.activeInHierarchy != setActive)
            {
                if (setActive)
                {
                    Apparition.instance.Activate(activeImmediately);
                }
                else
                {
                    Apparition.instance.Deactivate();
                }
            }
        }
    }
}

