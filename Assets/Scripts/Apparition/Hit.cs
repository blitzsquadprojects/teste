﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ApparitionSpace
{
    public class Hit : MonoBehaviour
    {
        void OnTriggerStay(Collider hit)
        {
            if(Apparition.instance == null)
            {
                return;
            }
            if (hit.CompareTag(Apparition.instance.tag))
            {
                Apparition.instance.Deactivate();
            }
        }
    }
}

