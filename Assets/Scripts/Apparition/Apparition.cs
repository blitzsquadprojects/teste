﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ApparitionSpace
{
    public class Apparition : MonoBehaviour
    {
        public static Apparition instance { get; protected set; }

        enum States
        {
            Disabled, Waiting, PrepareATK ,ATK
        }
        static States state = States.Disabled;

        Transform player;

        [SerializeField]
        GameObject model;

        [Header("Appear")]
        [SerializeField]
        float MaxAreaToAppear;
        [SerializeField]
        float MinAreaToAppear;

        [Space(10)]
        [SerializeField]
        float MaxTimeToAppear;
        [SerializeField]
        float MinTimeToAppear;

        [Header("Attack")]
        [SerializeField]
        float timeToATK;

        float currentTimeToATK, currentTimeToAppear;

        int tryApper = 0;

        void Start()
        {
            instance = this;
            gameObject.tag = "Apparition";
            model = transform.GetChild(0).gameObject;

            player = Player.instance.transform;

            gameObject.SetActive(false);
            tryApper = 0;
        }

        public void Activate(bool areaActivate)
        {
            state = States.Waiting;
            model.SetActive(false);
            gameObject.SetActive(true);

            if (areaActivate)
            {
                tryApper = 0;
                Appear();
                return;
            }

            currentTimeToAppear = Time.time + Random.Range(MinTimeToAppear, MaxTimeToAppear);
        }

        void Appear()
        {
            tryApper++;
            state = States.PrepareATK;
            Vector3 area = new Vector3(player.position.x + Random.Range(MinAreaToAppear, MaxAreaToAppear),
                                       transform.position.y,
                                       player.position.z + Random.Range(MinAreaToAppear, MaxAreaToAppear));

            transform.position = area;

            Vector3 rot = transform.eulerAngles;
            transform.LookAt(player);
            transform.eulerAngles = new Vector3(rot.x, transform.eulerAngles.y, transform.eulerAngles.z);

            if(tryApper < 50)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit))
                {
                    if (hit.transform.CompareTag("Player"))
                    {
                        currentTimeToATK = Time.time + timeToATK;
                        state = States.ATK;
                        model.SetActive(true);
                    }
                    else
                    {
                        Appear();
                    }
                }
                else
                {
                    Appear();
                }
            }
            else
            {
                Deactivate();
            }
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
            model.SetActive(false);
            state = States.Disabled;
        }

        void Update()
        {
            switch (state)
            {
                case States.Waiting:
                    if (Time.time > currentTimeToAppear)
                    {
                        tryApper = 0;
                        Appear();
                    }
                    break;
                case States.ATK:
                    if (Time.time > currentTimeToATK)
                    {
                        print("Atacou");
                        Deactivate();
                    }
                    break;
            }
        }
    }
}

