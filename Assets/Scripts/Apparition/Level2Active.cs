﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ApparitionSpace
{
    public class Level2Active : MonoBehaviour
    {
        [SerializeField]
        Interaction.level2.AtivarInteração oleo, manivela;

        Transform [] triggers;

        void Start()
        {
            triggers = new Transform[transform.childCount];
            for (int i = 0; i < transform.childCount; i++)
            {
                triggers[i] = transform.GetChild(i);
                triggers[i].gameObject.SetActive(false);
            }
        }

        
        void Update()
        {
            if(oleo.dialogo.activeInHierarchy || manivela.dialogo.activeInHierarchy)
            {
                Apparition.instance.Activate(false);
                for (int i = 0; i < transform.childCount; i++)
                {
                    triggers[i].gameObject.SetActive(true);
                }
            }
        }
    }
}

