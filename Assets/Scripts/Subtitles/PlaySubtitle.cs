﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlaySubtitle : MonoBehaviour
{
    private AudioSource audioSource;
    private ScriptManager scriptManager;
    private SubtitleGuiManager guiManager;
    private bool enter = false;
    private bool inArray = false;


    public static bool otherPlay = false;
    public static bool[] play = new bool[5];
    public static AudioSource[] audios = new AudioSource[5];
    

    public float[] LineDuration;
    public int limite;

    private void Awake()
    {
        scriptManager = FindObjectOfType<ScriptManager>();
        guiManager = FindObjectOfType<SubtitleGuiManager>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (enter == true)
        {
            for (int i = 0; i < audios.Length ; i++)
            {

                if (audios[i] != null && !play[i] && !otherPlay)
                {
                    play[i] = true;
                    StartCoroutine(PlayAudio(i));
                }
            }
        }
    }

    void PlayerEnter()
    {
        if (Triggers.trigger < limite && !enter)
        {
            Triggers.trigger++;
            Triggers.yes = true;
        }

        //colocar os áudios em um array
        for (int i = 0; i < audios.Length && !inArray; i++)
        {
            if (audios[i] == null)
            {
                audios[i] = audioSource;
                inArray = true;

                //Impedir que o jogador repita o mesmo áudio mais de uma vez
                enter = true;
            }
        }
    }

    public IEnumerator DoSubtitle(int i)
    {
        var script = scriptManager.GetText(audios[i].clip.name);
        int a = -1;

        if (audios[0] != null && audios[1] != null && audios[2] != null && audios[3] != null && audios[4] != null && i == 4)
        {
            for (int b = 0; b < audios.Length; b++)
            {
                audios[b] = null;
                play[b] = false;
            }
        }

        foreach (var line in script)
        {
            if (a < LineDuration.Length - 1)
            {
                a++;
            }

            guiManager.SetText(line);

            yield return new WaitForSeconds(LineDuration[a]);
        }

        guiManager.SetText(string.Empty);
        gameObject.SetActive(false);
        otherPlay = false;
    }


    public IEnumerator PlayAudio(int i)
    {
        audios[i].Play();
        otherPlay = true;
        StartCoroutine(DoSubtitle(i));
        yield return new WaitForSeconds(audios[i].clip.length);     
    }
}
