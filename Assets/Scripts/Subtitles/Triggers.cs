﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triggers : MonoBehaviour
{
    public static int trigger = -1;
    public static bool yes = false;
    public GameObject[] triggers;

    private void Start()
    {
        trigger = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (yes)
        {
            Debug.Log(trigger);
            triggers[trigger].SetActive(true);
            yes = false;
        }
    }

}
