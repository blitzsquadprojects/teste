﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    public static CanvasController instance { get; private set; }

    public Image[] slots;

    [Space(10)]
    [SerializeField]
    Text numPill;
    [SerializeField]
    Text numBattery;

    [SerializeField]
    Image life_Player;
    [SerializeField]
    float startScale, endScale;

    [SerializeField]
    Image [] battery;

    public Image actionButton;

    void Awake()
    {
        instance = this;
        actionButton.gameObject.SetActive(false);

        for (int i = 0; i < slots.Length; i++)
        {
            ChangeSlot(i, null);
        }

        life_Player.transform.localScale = Vector3.one * startScale;
    }

    void Update()
    {
        numPill.text = Player.num_Pill.ToString();
        numBattery.text = Player.num_Battery.ToString();

        float life = Mathf.InverseLerp(70, 0, Player.instance.current_Radioactive);
        float scale = Mathf.Lerp(startScale, endScale, life);

        life_Player.transform.localScale = Vector3.one * scale;
    }

    public void ChangeSlot(int pos, Sprite image)
    {
        if (image != null)
        {
            slots[pos].gameObject.SetActive(true);
            slots[pos].sprite = image;
        }
        else
        {
            slots[pos].gameObject.SetActive(false);
        }
    }

    public void ChangeActionButton(Sprite sprite)
    {
        if(sprite != null)
        {
            actionButton.gameObject.SetActive(true);
            actionButton.sprite = sprite;
        }
        else
        {
            actionButton.gameObject.SetActive(false);
        }
    }

    public void CheckBatery(float value, float max)
    {
        float x = max/battery.Length;

        for (int i = 0; i < battery.Length; i++)
        {
            if(value >= x * i)
            {
                if(i > 0)
                {
                    battery[i-1].gameObject.SetActive(false);
                }
                battery[i].gameObject.SetActive(true);
            }
            else
            {
                battery[i].gameObject.SetActive(false);
            }
        }
    }
}
