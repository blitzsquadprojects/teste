﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Geiger : MonoBehaviour
{
    [HideInInspector]
    public float radioactive_Receive;

    [SerializeField]
    float Max_Emitter, Max_Angle, Min_Angle;

    float radiation_Level;

    [SerializeField]
    Transform pointer;

    void Start()
    {
        
    }

    void Update()
    {
        radioactive_Receive = Player.instance.radioactive_Receive;

        radiation_Level = Mathf.InverseLerp(0, Max_Emitter, radioactive_Receive);

        Vector3 pointer_Rot = pointer.localEulerAngles;
        pointer_Rot.y = Mathf.Lerp(Min_Angle, Max_Angle, radiation_Level);

        pointer.localEulerAngles = pointer_Rot;
    }
}
