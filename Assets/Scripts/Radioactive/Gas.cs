﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerTrigger))]
[RequireComponent(typeof(BoxCollider))]
public class Gas : MonoBehaviour
{
    [SerializeField]
    float intensy_Radiation;

    [SerializeField]
    float Distance_toMax;
    BoxCollider boxCollider;

    float initial_Radioation;

    void Start()
    {
        initial_Radioation = Player.instance.radioactive_Receive;

        boxCollider = GetComponent<BoxCollider>();
        boxCollider.isTrigger = true;

        Vector3 center = boxCollider.center;
        center.y = 0;
        if (center != Vector3.zero)
        {
            Debug.LogError("O centro do collider tem que estar no centro do objeto");
        }
    }

    void PlayerStay(GameObject hit)
    {
        float i = intensy_Radiation;

        if (Distance_toMax > 0)
        {
            Vector3 playerPos = transform.InverseTransformPoint(hit.transform.position);
            float dX = Mathf.InverseLerp(boxCollider.size.x / 2, boxCollider.size.x / 2 - Distance_toMax, Mathf.Abs(playerPos.x));
            float dZ = Mathf.InverseLerp(boxCollider.size.z / 2, boxCollider.size.z / 2 - Distance_toMax, Mathf.Abs(playerPos.z));

            float d = (dX + dZ) / 2;
            i = Mathf.Lerp(initial_Radioation, intensy_Radiation, d);
        }
        Player.instance.radioactive_Receive = i;

        if(initial_Radioation == intensy_Radiation)
        {
            initial_Radioation = intensy_Radiation;
        }
    }

    void PlayerExit()
    {
        Player.instance.radioactive_Receive = Player.instance.middle_Recieve;
    }

    void PlayerEnter()
    {
        initial_Radioation = Player.instance.radioactive_Receive;
    }
}
