﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Estante : MonoBehaviour
{
    public static bool correto = false;
    private bool col = false;
    private bool dir = false;
    private bool esq = false;
    private Rigidbody rb;
    
   
    // Start is called before the first frame update
    void Start()
    {
        correto = false;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
            if (col)
            {
                if (Input.GetKeyUp(KeyCode.E))
                {
                    MoverEstante();
                }
            }

    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            col = true;
           

            if(other.transform.position.z > gameObject.transform.position.z)
            {
                esq = true;
            }

            if (other.transform.position.z < gameObject.transform.position.z)
            {
                dir = true;
            }
        }

        if(other.gameObject.CompareTag("ColliderArmario"))
        {
            correto = true;
            Debug.Log("yes");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") )
        {
            col = false;
            esq = false;
            dir = false;
        }

        if (other.gameObject.CompareTag("ColliderArmario"))
        {
            correto = false;
            Debug.Log("no");
        }
    }

    void MoverEstante()
    {
        if (esq)
        {
            rb.velocity = new Vector3(0, 0, -100);
        }

        else if (dir )
        {
            rb.velocity = new Vector3(0, 0, 100);
        }
    }
}

