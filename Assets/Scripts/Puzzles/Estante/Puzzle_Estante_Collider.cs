﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Estante_Collider : MonoBehaviour
{
    private bool col = false;
    public static bool completo = false;
    public GameObject dialogo;
    public GameObject mascaraNova;
    public GameObject mascaraVelha;
    public GameObject barradeFerro;

    void Start()
    {
        completo = false;
    }

    void Update()
    {
        if (col)
        {
            if (Input.GetKeyUp(KeyCode.E) && !completo && dialogo.activeSelf)
            {
                if (Puzzle_Estante.correto == true)
                {
                    barradeFerro.SetActive(true);
                    mascaraNova.SetActive(true);
                    mascaraVelha.SetActive(false);
                    completo = true;
                    CanvasController.instance.ChangeSlot(1, null);
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            col = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            col = false;
        }
    }

    void ResetCode()
    {
        barradeFerro.SetActive(false);
        mascaraNova.SetActive(false);
        mascaraVelha.SetActive(true);
        completo = false;
    }
}

