﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle_Caixa : MonoBehaviour
{
    // public Slider[] sliders;
    public GameObject[] sliders;
    public int[] respostas;

    public Collider oleoCol;

    public GameObject cameraPlayer;
    public GameObject cameraPuzzle;
    public GameObject tampa;
    public GameObject tampaAberta;

    private bool CameraAtivada = false;
    private bool col;

    private int[] position;
    private int coluna = 0;
    private bool puzzleCompleto = false;

    [SerializeField]
    Sprite action;

    // Start is called before the first frame update
    void Start()
    {
        position = new int[sliders.Length];
    }

    // Update is called once per frame
    void Update()
    {
      

        if (col)
        {
            if (CanvasController.instance != null && !CameraAtivada)
            {
                CanvasController.instance.ChangeActionButton(action);
            }

            if (Input.GetKeyUp(KeyCode.E) && !puzzleCompleto)
            {
                CanvasController.instance.ChangeActionButton(null);
                Camera();
            }
        }

        else CanvasController.instance.ChangeActionButton(null);

        if (CameraAtivada && !puzzleCompleto)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) && position[coluna] > 0)
            {
                position[coluna]--;
                sliders[coluna].transform.position = new Vector3(sliders[coluna].transform.position.x + 0.42f, sliders[coluna].transform.position.y, sliders[coluna].transform.position.z);
            }

            if (Input.GetKeyDown(KeyCode.DownArrow) && position[coluna] < position.Length-1)
            {
                position[coluna]++;
                sliders[coluna].transform.position = new Vector3(sliders[coluna].transform.position.x - 0.42f, sliders[coluna].transform.position.y, sliders[coluna].transform.position.z);
            }

            if (Input.GetKeyDown(KeyCode.RightArrow) && coluna < sliders.Length -1)
            {
                coluna++;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow) && coluna > 0)
            {
                coluna--;
            }

            if(position[0] == respostas[0])
            {
                Debug.Log("1 Correto");
            }

            if (position[1] == respostas[1])
            {
                Debug.Log("2 Correto");
            }

            if (position[2] == respostas[2])
            {
                Debug.Log("3 Correto");
            }

            if (position[0] == respostas [0] && position[1] == respostas[1] && position[2] == respostas[2])
            {
                Debug.Log("Puzzle Completo");
                cameraPlayer.SetActive(true);
                cameraPuzzle.SetActive(false);
                oleoCol.enabled = true;
                puzzleCompleto = true;
                tampa.SetActive(false);
                tampaAberta.SetActive(true);
                Time.timeScale = 1;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            col = true;
        }
    }


        private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            col = false;
        }
    }

    IEnumerator CanvasGroupFadeIn(CanvasGroup CG, float time)
    {
        while (CG.alpha < 1)
        {
            CG.alpha += Time.deltaTime / time;
            yield return new WaitForEndOfFrame();
        }
    }

    public void Camera()
    {
        if (!CameraAtivada)
        {
            cameraPuzzle.SetActive(true);
            cameraPlayer.SetActive(false);
            CameraAtivada = true;
            Time.timeScale = 0;
        }

        else if (CameraAtivada)
        {
            cameraPlayer.SetActive(true);
            cameraPuzzle.SetActive(false);
            CameraAtivada = false;
            Time.timeScale = 1;
        }
    }
}
