﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Canos : MonoBehaviour
{
    public GameObject valvula;
    public int posicaoCorreta;
    public GameObject filtro;
    public Collider colfiltro;
    public GameObject poscorreta;
    public GameObject posErrada;
   

    private int posicaoAtual = 1;

    private int rot;
    private bool correto = false;
    private bool col = false;
    private static int corretos = 0;
    public static bool puzzleterminado;

    private void Start()
    {
        corretos = 0;
        puzzleterminado = false;
    }

    private void Update()
    {
        if (col && !puzzleterminado && AtivarPuzzledosCanos.canosAtivados)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
               GirarValvula();
            }
        }

        if (corretos == 3 )
        {
            colfiltro.enabled = true;
            filtro.SetActive(true);
            puzzleterminado = true;
            CanvasController.instance.ChangeSlot(1, null);
            Debug.Log("Puzzle Completo");
        }

    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Player") && !puzzleterminado)
        {
            col = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !puzzleterminado)
        {
            col = false;
        }
    }

    void GirarValvula()
    {
        rot += 45;
        valvula.transform.rotation = Quaternion.Euler(rot, valvula.transform.rotation.y, valvula.transform.rotation.z);

        if (posicaoAtual >= 4)
        {
            rot = 0;
            posicaoAtual = 0;
        }
            
            posicaoAtual++;

        if (posicaoAtual == posicaoCorreta)
        {
            posErrada.SetActive(false);
            poscorreta.SetActive(true);
            correto = true;
            corretos++;
        }

        else if(correto)
        {
            posErrada.SetActive(true);
            poscorreta.SetActive(false);
            correto = false;
            corretos--;
        }

        Debug.Log("posicao Atual:" + posicaoAtual);
        Debug.Log("corretos:" + corretos);
    }
}
