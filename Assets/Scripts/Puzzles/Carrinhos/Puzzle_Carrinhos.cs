﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Carrinhos : MonoBehaviour
{

    public Collider col;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ColliderCarrinho"))
        {
            col.enabled = true;
            Debug.Log("PuzzleConcluído");
        }
    }
}
