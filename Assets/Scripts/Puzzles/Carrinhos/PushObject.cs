﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushObject : MonoBehaviour
{
    private float pushStrength = 10;
    private bool colZ;
    private bool zmais;
    private bool zmenos;
    private bool colX;
    private bool xmais;
    private bool xmenos;
    private Rigidbody rb;
    private Rigidbody rbatual;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (colZ)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                MoverZ();
            }
        }

        else if (colX)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                MoverX();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        

        if (other.gameObject.CompareTag("CarrinhoZ") && !colZ && !colX)
        {
            rbatual = other.GetComponent<Rigidbody>();
            rb = rbatual;
            colZ = true;

            if (other.transform.position.z > gameObject.transform.position.z)
            {
                zmais = true;
            }

            if (other.transform.position.z < gameObject.transform.position.z)
            {
                zmenos = true;
            }
        }

        if (other.gameObject.CompareTag("CarrinhoX") && !colZ && !colX)
        {
            rbatual = other.GetComponent<Rigidbody>();
            rb = rbatual;

            colX = true;

            if (other.transform.position.x > gameObject.transform.position.x)
            {
                xmais = true;
            }

            if (other.transform.position.x < gameObject.transform.position.x)
            {
                xmenos = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("CarrinhoZ"))
        {
            colZ = false;
            zmais = false;
            zmenos = false;
        }

        if (other.gameObject.CompareTag("CarrinhoX"))
        {
            colX = false;
            xmais = false;
            xmenos = false;
        }
    }



    private void MoverZ()
    {
        if(zmais)
        {
            rb.velocity = new Vector3(0, 0, 150);
        }

        else if(zmenos)
        {
            rb.velocity = new Vector3(0, 0, -150);
        }
    }

    private void MoverX()
    {
        if (xmais)
        {
            rb.velocity = new Vector3(150, 0, 0);
        }

        else if (xmenos)
        {
            rb.velocity = new Vector3(-150, 0, 0);
        }
    }
}
