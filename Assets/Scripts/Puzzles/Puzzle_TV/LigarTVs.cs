﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LigarTVs : MonoBehaviour
{
    public GameObject Tela1_Tv1;
    public GameObject Tela2_Tv1;
    public GameObject Tela1_Tv2;
    public GameObject Tela2_Tv2;
    public GameObject Tela1_Tv3;
    public GameObject Tela2_Tv3;

    public static bool Tvs_Ligadas;

    // Start is called before the first frame update
    void Start()
    {
        Tvs_Ligadas = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            Tela1_Tv1.SetActive(false);
            Tela1_Tv2.SetActive(false);
            Tela1_Tv3.SetActive(false);
            Tela2_Tv1.SetActive(true);
            Tela2_Tv2.SetActive(true);
            Tela2_Tv3.SetActive(true);
            Tvs_Ligadas = true;
            gameObject.SetActive(false);
        }
    }
}
