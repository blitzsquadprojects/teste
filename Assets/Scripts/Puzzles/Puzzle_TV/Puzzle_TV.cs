﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_TV : MonoBehaviour
{
    
    public GameObject[] canais;
    public int canalCorreto;
    public GameObject tela1;
    public GameObject ChavedeBoca;

    private int canalAtual = 0;
    private bool correto = false;
    private bool col = false;
    private bool terminado;
    private static int corretos = 0;
    private static bool puzzleTerminado;

    private void Start()
    {
        puzzleTerminado = false;

        if (canalAtual == canalCorreto)
        {
            corretos = 1;
            correto = true;
        }
    }

    private void Update()
    {
        if(col && LigarTVs.Tvs_Ligadas && !puzzleTerminado)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                MudarCanal();
            }
        }

        if (corretos == 3 && !terminado)
        {
            puzzleTerminado = true;
            terminado = true;
            canais[canalAtual].SetActive(false);
            tela1.SetActive(true);
            ChavedeBoca.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            col = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            col = false;
        }
    }

    void MudarCanal()
    {
        canalAtual++;

        if (canalAtual >= canais.Length)
        {
            canalAtual = 0;
            canais[canalAtual].SetActive(true);
            canais[canais.Length - 1].SetActive(false);
        }

        if (canalAtual > 0)
        {
            canais[canalAtual - 1].SetActive(false);
        }

        canais[canalAtual].SetActive(true);

        if (canalAtual == canalCorreto)
        {
            corretos += 1;
            correto = true;
        }

        else if(correto)
        {
            correto = false;
            corretos--;
        }
    }

    void ResetCode()
    {
        puzzleTerminado = false;

        if (canalAtual == canalCorreto)
        {
            corretos = 1;
            correto = true;
        }
    }
}
