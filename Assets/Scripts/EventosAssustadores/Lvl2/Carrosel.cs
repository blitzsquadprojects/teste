﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carrosel : MonoBehaviour
{
    public GameObject carrosel;
    public Collider col;
    private bool rodar;
    private float speed;

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if(rodar)
        {
            speed += Time.deltaTime * -10;
            carrosel.transform.rotation = Quaternion.Euler(carrosel.transform.rotation.x, speed, carrosel.transform.rotation.z);


            if (speed < -100)
            {
                rodar = false;
            }
        }
    }

    void PlayerEnter()
    {
        rodar = true;
    }

    void PlayerExit()
    {
        col.enabled = false;
    }
}
