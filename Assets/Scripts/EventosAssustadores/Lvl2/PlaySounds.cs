﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySounds : MonoBehaviour
{
    private AudioSource audioSource;
    private bool play;
    private float cont;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponentInChildren<AudioSource>();
        play = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(play)
        {
            cont += Time.deltaTime;

            if(cont >= audioSource.clip.length)
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && !play)
        {
            audioSource.Play();
            play = true;
        }
    }
}
