﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carrinho : MonoBehaviour
{
    private Rigidbody rb;
    private bool yes;
    private bool no;

    // Start is called before the first frame update
    void Start()
    { 
        yes = false;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    { 
        if (yes && !no)
        {
            rb.velocity = new Vector3(60, 0, 0);

            if(gameObject.transform.position.x > 100)
            {
                no = true;
            }
        }
    }

    void PlayerEnter()
    {
        if (!yes)
        {
          yes = true;
        }
    }
}
