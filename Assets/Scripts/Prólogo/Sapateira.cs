﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sapateira : MonoBehaviour
{
    public GameObject sapatos;
    public GameObject fala;
    public GameObject bloqueio;

    [SerializeField]
    Sprite action;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayerStay()
    {
        if (CanvasController.instance != null)
        {
            CanvasController.instance.ChangeActionButton(action);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            sapatos.SetActive(true);
            fala.SetActive(true);
            bloqueio.SetActive(false);
            CanvasController.instance.ChangeActionButton(null);
            gameObject.SetActive(false);
        }
    }

    void PlayerExit()
    {
        CanvasController.instance.ChangeActionButton(null);
    }
}
