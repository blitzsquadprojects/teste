﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cam : MonoBehaviour
{
    Transform[] ways;
    public GameObject wayfather;
    public float speed;
    int indexway = 0;
    public GameObject carregando;
    // Start is called before the first frame update

    void Start()
    {
        ways = wayfather.GetComponentsInChildren<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (indexway < ways.Length)
        {
            Vector3 dir = ways[indexway].position - transform.position;
            Quaternion newrot = Quaternion.LookRotation(dir);
            transform.rotation = Quaternion.Lerp(transform.rotation, newrot, Time.deltaTime);
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
            if (Vector3.Distance(transform.position, ways[indexway].position) < 3 && indexway < ways.Length)
            {
                indexway++;

                if (indexway == ways.Length)
                {
                    carregando.SetActive(true);
                }
            }
        }
    }
}
