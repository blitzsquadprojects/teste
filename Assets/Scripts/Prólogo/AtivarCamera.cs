﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtivarCamera : MonoBehaviour
{
    public GameObject player;
    public GameObject cameraPlayer;
    public GameObject cam;
    
   

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayerEnter()
    {
            cam.transform.position = cameraPlayer.transform.position;
            cam.transform.rotation = cameraPlayer.transform.rotation;
            cam.SetActive(true);
            player.SetActive(false);
 
    }
}
