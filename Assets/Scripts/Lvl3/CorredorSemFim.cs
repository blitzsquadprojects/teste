﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorredorSemFim : MonoBehaviour
{
    public GameObject player;
    public GameObject position;
    public GameObject abrirPorta;
    public GameObject PortaFechada;
    public GameObject PortaAberta;
    private float cont;
    private bool col;

    // Start is called before the first frame update
    void Start()
    {
        cont = 15;
    }

    // Update is called once per frame
    void Update()
    {
        if (col)
        {
            cont -= Time.deltaTime;

            if(cont < 0)
            {
                abrirPorta.SetActive(true);
            }
        }
    }

    void PlayerEnter()
    {
        PortaAberta.SetActive(false);
        PortaFechada.SetActive(true);
        player.transform.position = new Vector3 (player.transform.position.x,player.transform.position.y,position.transform.position.z);
        col = true;
    }
}
