﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Berço : MonoBehaviour
{
    public GameObject berço;
    public AudioSource baby;
    private bool col;
    private float cont;
    private float speed;

    // Start is called before the first frame update
    void Start()
    {
        cont = 15;
    }

    // Update is called once per frame
    void Update()
    {
        if(col)
        {
            cont -= Time.deltaTime;
            speed += Time.deltaTime * - 2;


            if(cont < 5 && !baby.isPlaying)
            {
                baby.Play();
            }

            if(cont < 0)
            {
                gameObject.SetActive(false);
            }

            berço.transform.rotation = Quaternion.Euler(speed, berço.transform.position.y, berço.transform.position.z);


        }
    }

    void PlayerEnter()
    {
        col = true;
    }
}
