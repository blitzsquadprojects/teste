﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParaSalinhaDeOlhos : MonoBehaviour
{
    public GameObject imageFade;

    private Animator animFade;
    private float cont = 0.8f;
    private bool yes;


    // Start is called before the first frame update
    void Start()
    {
        animFade = imageFade.GetComponent<Animator>();
    }

    private void Update()
    {

        if (yes)
        {
            cont -= Time.deltaTime;


            if (cont <= 0)
            {
                animFade.SetBool("Fade", false);
                animFade.SetBool("FadeIn", true);
                yes = false;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            animFade.SetBool("Fade", true);
            yes = true;
        }
    }

}
