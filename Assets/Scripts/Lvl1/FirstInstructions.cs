﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstInstructions : MonoBehaviour
{
    void Start()
    {
        ObjectiveController.NextInstruction();
        StartCoroutine(NextInst(5));
    }

    void Update()
    {
        
    }

    IEnumerator NextInst(float t)
    {
        yield return new WaitForSeconds(t);
        ObjectiveController.NextInstruction();
        StartCoroutine(NextObjs(5));

    }

    IEnumerator NextObjs(float t)
    {
        yield return new WaitForSeconds(t);
        ObjectiveController.NextObjective();
        print("A");
    }
}
