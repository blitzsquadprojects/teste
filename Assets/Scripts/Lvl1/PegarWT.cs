﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PegarWT : MonoBehaviour
{
    public GameObject dialogo;
    public GameObject ExtraTrigger;

    [SerializeField]
    Sprite action;

    void Start()
    {
        
    }

    void Update()
    {
        
    }
    void PlayerStay()
    {
        if (CanvasController.instance != null)
        {
            CanvasController.instance.ChangeActionButton(action);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            dialogo.SetActive(true);
            CanvasController.instance.ChangeActionButton(null);
            ExtraTrigger.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    void PlayerExit()
    {
        CanvasController.instance.ChangeActionButton(null);
    }
}
