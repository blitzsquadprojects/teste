﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtivarPuzzledosCanos : MonoBehaviour
{
    public static bool canosAtivados;

    // Start is called before the first frame update
    void Start()
    {
        canosAtivados = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void PlayerEnter()
    {
        canosAtivados = true;
        gameObject.SetActive(false);
    }

    void ResetCode()
    {
        canosAtivados = false;
        gameObject.SetActive(true);
    }
}
