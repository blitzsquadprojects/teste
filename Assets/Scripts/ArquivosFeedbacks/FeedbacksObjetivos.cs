﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedbacksObjetivos : MonoBehaviour
{
    public static bool FeedbackObjetivoBool = false;
    public static bool feedbackComplementarBool = false;

    public static Text feedbackObjetivo;
    public static Text feedbacksComplementares;

    public static string txtObjetivoAtual;
    public static string txtFeedbacksComplementares;

    //public float repetirFeedbackAtual = 0; 

    public float tempoFeedback;
    public Text txtFeedbackNaTela;
    public Text txtFeedbackComplementarNaTela;

    //private float contagemFeedback = 0;

    // Start is called before the first frame update
    void Awake()
    {
        feedbackObjetivo = txtFeedbackNaTela;
        feedbacksComplementares = txtFeedbackComplementarNaTela;
    }

    // Update is called once per frame
    void Update()
    {
        //Exibe o objetivo atual
        if (FeedbackObjetivoBool)
        {
            StartCoroutine(AtivandoFeedback(tempoFeedback));
        }
        //Exibe o objetivo atual quando clica TAB
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            FeedbackObjetivoBool = true;
        }
        //Exibe o feedback complementar
        if (feedbackComplementarBool)
        {
            StartCoroutine(FeedbacksComplementares(tempoFeedback));
        }

    }
    void FixedUpdate()
    {
        //contagemFeedback++;        
    }
    //Exibe e depois esconde o objetivo atual
    IEnumerator AtivandoFeedback(float segundos)
    {
        feedbackObjetivo.gameObject.SetActive(true);
        feedbacksComplementares.gameObject.SetActive(false);

        txtFeedbackNaTela.text = txtObjetivoAtual;
        FeedbackObjetivoBool = false;
        yield return new WaitForSeconds(segundos);

        feedbackObjetivo.gameObject.SetActive(false);
        //Debug.Log(txtObjetivoAtual);
        
        //yield return new WaitForSeconds(segundos);
        //TextoParaFeedback.ativandoFeedback = false;
    }
    //Exibe e depois esconde o feedback complementar
    IEnumerator FeedbacksComplementares(float segundos)
    {
        feedbackObjetivo.gameObject.SetActive(false);
        feedbacksComplementares.gameObject.SetActive(true);

        txtFeedbackComplementarNaTela.text = txtFeedbacksComplementares;
        //Debug.Log(txtObjetivoAtual + "   Deu certo");
        feedbackComplementarBool = false;
        yield return new WaitForSeconds(segundos);

        feedbacksComplementares.gameObject.SetActive(false);
        //Debug.Log(txtObjetivoAtual);

        //yield return new WaitForSeconds(segundos);
        //TextoParaFeedback.ativandoFeedback = false;
    }


}
