﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerTrigger))]
public class ActiveFeedback : MonoBehaviour
{
    void PlayerEnter()
    {
        ObjectiveController.NextObjective();
    }
}
