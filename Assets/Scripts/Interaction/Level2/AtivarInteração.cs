﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Interaction
{
    namespace level2
    {
        [RequireComponent(typeof(PlayerTrigger))]
        public class AtivarInteração : MonoBehaviour
        {
            public ObjectReceiver objectReceiver;

            public GameObject dialogo;

            
            void Start()
            { 
                objectReceiver.gameObject.SetActive(false);
            }

            void PlayerStay()
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    objectReceiver.gameObject.SetActive(true);
                    if(dialogo != null)
                    {
                        dialogo.SetActive(true);
                    }
                }
            }

            void ResetCode()
            {
                objectReceiver.gameObject.SetActive(false);
                dialogo.SetActive(false);
            }
        }
    }
}
