﻿using UnityEngine;

namespace Interaction
{
    public class Inventory : MonoBehaviour
    {
        public static ObjectReceiver[] objects;

        public static int MaxObjects;

        public static KeyCode[] keys;

        void Start()
        {
            MaxObjects = CanvasController.instance.slots.Length;

            objects = new ObjectReceiver[MaxObjects];
            keys = new KeyCode[MaxObjects];

            for (int i = 0; i < keys.Length; i++)
            {
                int keyNum = (int)KeyCode.Alpha0 + 1 + i;
                keys[i] = (KeyCode)keyNum;
            }
        }

        void Update()
        {
            if (Input.anyKey && int.TryParse(Input.inputString, out int key))
            {
                key--;
                if (key >= 0 && key < objects.Length && objects[key] != null)
                {
                    objects[key].DiscardedObj(Player.instance.transform.position, key);
                    objects[key] = null;
                    CanvasController.instance.ChangeSlot(key, null);
                }

                /*
                for (int i = 0; i < keys.Length; i++)
                {
                    if (Input.GetKeyDown(keys[i]))
                    {
                        print(keys[i]);
                    }
                }*/
            }
        }

        public static int ReceiveNewObject(ObjectReceiver newObject, Sprite image)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i] == null)
                {
                    objects[i] = newObject;
                    CanvasController.instance.ChangeSlot(i, image);
                    return i;
                }
            }
            //GameController.instance;
            return -1;
        }

        public static void ResetInventory()
        {
            for(int i = 0; i < objects.Length; i++)
            {
                if(objects[i] != null)
                {
                    objects[i].gameObject.SendMessage("ResetCode", SendMessageOptions.DontRequireReceiver);

                }
            }
        }
    }
}

