﻿using UnityEngine;

namespace Interaction
{
    namespace level1
    {
        [RequireComponent(typeof(PlayerTrigger))]
        public class ActiveMaskManual : MonoBehaviour
        {
            [SerializeField]
            ObjectReceiver objectReceiver;

            void Start()
            {
                objectReceiver.gameObject.SetActive(false);
            }

            void PlayerStay()
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Player.instance.canMove = false;
                    objectReceiver.gameObject.SetActive(true);
                }
            }
        }
    }
}

