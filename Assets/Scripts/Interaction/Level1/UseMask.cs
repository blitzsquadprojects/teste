﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Interaction
{
    namespace level1
    {
        [RequireComponent(typeof(PlayerTrigger))]
        public class UseMask : MonoBehaviour
        {
            [SerializeField]
            UnityEngine.UI.Text explain;

            [SerializeField]
            string [] explanations;
            int i = 0;

            void PlayerStay()
            {
                Player.instance.canMove = false;
                if (i < explanations.Length)
                {
                    explain.gameObject.SetActive(true);
                    explain.text = "Aperte " + (i + 1) + "para usar " + explanations[i];
                }
                else
                {
                    explain.gameObject.SetActive(false);
                }

                if (Input.GetKeyDown(Inventory.keys[i + 1]))
                {
                    i++;
                }
            }
        }
    }
}
