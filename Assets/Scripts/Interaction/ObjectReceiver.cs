﻿using UnityEngine;

namespace Interaction
{
    [RequireComponent(typeof(PlayerTrigger))]
    public class ObjectReceiver : MonoBehaviour
    {
        [SerializeField]
        GameObject interactiveObj;

        [SerializeField]
        ObjectSender[] objectSender;

        [HideInInspector]
        public int[] objPos;

        int amountObj = 0;

        void Awake()
        {
            StartReceiver();
        }

        void PlayerStay(GameObject hit)
        {
            if (Input.anyKeyDown)
            {
                for (int i = 0; i < objectSender.Length; i++)
                {
                    if (objPos[i] >= 0 && Input.GetKeyDown(Inventory.keys[objPos[i]]))
                    {
                        objPos[i] = -1;
                        amountObj++;

                        if (amountObj >= objectSender.Length)
                        {
                            if(interactiveObj != null)
                            {
                                if (interactiveObj.gameObject.scene.name == null)
                                {
                                    Instantiate(interactiveObj, hit.transform.position, hit.transform.rotation);
                                }

                                interactiveObj.SetActive(true);
                                interactiveObj.SendMessage("Result", SendMessageOptions.DontRequireReceiver);
                            }
                            gameObject.SetActive(false);
                        }
                    }
                }
            }
        }

        public void DiscardedObj(Vector3 playerPos, int key)
        {
            if (objPos[key] >= 0)
            {
                objectSender[key].transform.position = playerPos;
                objectSender[key].gameObject.SetActive(true);
                objPos[key] = -1;
            }
        }

        void StartReceiver()
        {
            amountObj = 0;
            objPos = new int[objectSender.Length];

            for (int i = 0; i < objectSender.Length; i++)
            {
                objectSender[i].Receiver = this;
                objectSender[i].posReceiver = i;
                objPos[i] = -1;
            }
        }

        void ResetCode()
        {
            StartReceiver();
            interactiveObj.SendMessage("ResetCode", SendMessageOptions.RequireReceiver);
        }
    }
}
