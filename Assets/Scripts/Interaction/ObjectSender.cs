﻿using UnityEngine;

namespace Interaction
{
    [RequireComponent(typeof(PlayerTrigger))]
    public class ObjectSender : MonoBehaviour
    {
        [HideInInspector]
        public ObjectReceiver Receiver;
        [HideInInspector]
        public int posReceiver;

        [SerializeField]
        Sprite image, action;

        [SerializeField]
        bool nextObjective = false;
        void PlayerStay()
        {
            if (CanvasController.instance != null)
            {
                CanvasController.instance.ChangeActionButton(action);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                CanvasController.instance.ChangeActionButton(null);
                Receiver.objPos[posReceiver] = Inventory.ReceiveNewObject(Receiver, image);

                if (Receiver.objPos[posReceiver] >= 0)
                {
                    gameObject.SetActive(false);
                    if (nextObjective)
                    {
                        ObjectiveController.NextObjective();
                        print("A");
                    }
                }
            }
        }

        void PlayerExit()
        {
            CanvasController.instance.ChangeActionButton(null);
        }
    }
}
