﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodaGigante : MonoBehaviour
{
    public GameObject ursinhoVelho;
    public GameObject ursinhoNovo;
    public GameObject roda;
    public GameObject manivela;

    private bool col;
    private float speed;
    private bool rodar;

    [SerializeField]
    Sprite action;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (rodar)
        {
            speed += Time.deltaTime * 10;
            roda.transform.rotation = Quaternion.Euler(roda.transform.rotation.x + speed, roda.transform.rotation.y, roda.transform.rotation.z);

            if (speed > 15)
            {
                CanvasController.instance.ChangeActionButton(null);
                gameObject.SetActive(false);
            }
        }

        if (col && !rodar)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                
                ursinhoVelho.SetActive(false);
                ursinhoNovo.SetActive(true);
                manivela.SetActive(true);
                rodar = true;
                CanvasController.instance.ChangeActionButton(null);
                ObjectiveController.NextObjective();
            }

            else if (CanvasController.instance != null)
            {
                CanvasController.instance.ChangeActionButton(action);
            }  
        }

        else CanvasController.instance.ChangeActionButton(null);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            col = true;
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            col = false;
        }
    }
}
