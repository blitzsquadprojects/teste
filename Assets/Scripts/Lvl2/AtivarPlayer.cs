﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtivarPlayer : MonoBehaviour
{
    public GameObject playerOld;
    public GameObject playerNew;

    public void AtivarJogador()
    {
        playerOld.SetActive(false);
        playerNew.SetActive(true);
    }
}
