﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{ 
    private AudioSource heart;
    public AudioSource Ambient;

    private bool fim;

    void Start()
    {
        heart = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!fim)
        {
            if (other.CompareTag("Heart1"))
            {
                if (heart.isPlaying == false)
                {
                    heart.Play();
                    Ambient.volume = 0.4f;
                }

                heart.pitch = 1;
            }

            if (other.CompareTag("Heart2"))
            {
                heart.pitch = 1.3f;
            }

            if (other.CompareTag("Heart3"))
            {
                heart.pitch = 1.6f;
            }

            if (other.CompareTag("Heart4"))
            {
                heart.pitch = 2f;
            }

            if (other.CompareTag("Heart5"))
            {
                heart.pitch = 1;
                fim = true;
            }
        }

        if(other.CompareTag("Heart6"))
        {
            gameObject.SetActive(false);
        }
    }
}
