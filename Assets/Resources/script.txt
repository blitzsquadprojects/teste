{
  "lines":
[

{
"key":"line1",
"line" : ["Oi! Boris? É você filho? Tire os sapatos antes de entrar!"]
},

{
"key":"line2",
"line" : ["Sim papai!"," ","Hmm... Onde está a sapateira?"]
},

{
"key":"line3",
"line" : [" ","Hmm... Que fome!","Pai? Posso comer um pedaço de pão?"]
},

{
"key":"line4",
"line" : ["Pode, filho. Está na cozinha! Não vá se entupir, hein!"]
},

{
"key":"line5",
"line" : ["Lushka! O que está fazendo aí seu ursinho guloso?","Quer um pão também? Vou pegar para nós, espere aí!"]
},

{
"key":"line6",
"line" : ["Bom dia Chernobyl e Pripyat! Aqui é Dmitri Vladimirovich, apresentando seu programa matinal de notícias!","Hoje é 25 de abril, o dia amanhece fresco como a primavera!","OY! OPA! NOTÍCIA DE ÚLTIMA HORA! HÁ UM VAZAMENTO NO REATOR 4, TODOS EVACUEM DA CIDADE IMEDIATAMENTE!"]
},












{
"key":"Lvl1_1",
"line" : [" ","Chernobyl..."]
},

{
"key":"Lvl1_2",
"line": ["Só uma foto, uma prova..."," Entre, pegue, saia. Simples!"]
},

{
"key":"Lvl1_3",
"line":["Eu não devia estar aqui..."]
},

{
"key":"Lvl1_4",
"line":["Alô? Alô? Alguém aí?","Mas... o que?"," Alguém na escuta? Câmbio."]
},

{
"key":"Lvl1_5",
"line":["A...Alô?"," Ah! Ótimo! Sabia que você ouviria!"," Escuta, eu preciso que você faça uma coisa. É extremamente importante!"," Como sabia que eu escutaria?","Como sabe que eu tô aqui?! Que história é essa?"," ","O rádio estava ligado, cabeção."," Agora, não fique aí parado! Preciso que você me ajude."," Tem uma loja de ferramentas logo mais para o meio da cidade. Vá até lá!"," Mas... Eu nem sei onde é!"," Ai ai ai... Claaro que sabe."]
},


{
"key":"Lvl1_6",
"line":["É.. Voz? Tem um... Caminhão enorme bloqueando a passagem...","Ah, tem razão, tem razão. Me esqueci desse detalhe.","Você terá de vir pela floresta. Há uma passagem lá.","Volte e vire a direita lá haverá uma abertura nas árvores."," "]
},

{
"key":"Lvl1_7",
"line":["Acho... Acho que eu cheguei...","Ótimo, ótimo! Agora. Aí dentro...É isso! Procure por uma chave de boca."]
},

{
"key":"Lvl1_8",
"line":["Onde está você? Podemos conversar?","É... Não vai dar.","Mas como!?... Alô? Alô... Oy Blin!"]
},

{
"key":"Lvl1_9",
"line":["Finalmente, aqui está! O que é que eu faço com isso?","Estava aí mesmo? Que ótimo! Tá legal, agora só falta mais uma coisa...","Ei! Mas e a sua chave de boca? O que eu faço com ela?","Por agora? Nada! Guarde-o. Vai precisar mais tarde.","Onde estava mesmo? Ah sim! Consegue ver os correios?"," Lá tem mais uma coisinha que eu preciso que pegue pra mim.","É melhor você saber o que “tá” fazendo..."]
},


{
"key":"Lvl1_10",
"line":["Ehh.. Voz?","Sim?","Você... Bem... Por que você tá aqui?"," ","Porque estou... Ora! Cresci aqui!","Mas... E... depois do acidente?","Bom... Assim como todos, precisei ir embora, é claro..."," Mas voltar aqui sempre me traz paz."]
},


{
"key":"Lvl1_11",
"line":["Oy, Blin!... Já está quase aí dentro, não está?"," Em uma das salas você deve achar uma máscara de gás.","Tem certeza que eu preciso mesmo de uma... máscara de gás?","Quer dizer... Não é meio... Ajuda mesmo contra a radiação?","Se você se acha tão esperto, siga sem mim, valentão!","Não... Espera! Eu não conheço nada daqui... Eu vou pegar seu treco...","SEU treco."]
},

{
"key":"Lvl1_12",
"line":["Mas! O que diabo está acontecendo aqui?","Você tem baterias o suficiente?","Suficiente pra quê?","Afastar a escuridão, é claro."," As coisas mais horríveis escondem-se no escuro para não serem vistas."," Ou será que somos nós que escondemos as coisas horríveis no escuro?"," Afinal, com um pouco de luz o escuro deixa, fundamentalmente, de existir..."]
},

{
"key":"Lvl1_13",
"line":["Okay, aqui está! A “minha” máscara de gás.","Agora sim! Está preparado para ver uma das maravilhas soviéticas?"," Vá até a praça Lenin, lá encontrará o “Palácio da Cultura Energética”! É difícil de não ver...","Ah “Maravilha”... ","Ei! Eu ouvi isso!"," ","Mas... Eu... Nem apertei... O rádio!"]
},

{
"key":"Lvl1_14",
"line":["E aí? Ela não é linda?","É... Pitoresco...","Okay, Agora, uma vez lá dentro, você precisa encontrar o banheiro."," Lá, finalmente, vai poder usar sua chave de boca e pegar um filtro para sua máscara.","E onde eu uso a chave??","Se eu não estou enganado, dentro de um grande cano tem um filtro emperrado."," Use a chave para abrir o cano, pegue e acople o filtro na sua máscara."," Dessa forma você pode seguir para o parque.","Mas! Eu quero encontrar uma foto... É só isso! Porque eu iria pro parque?","Pra eu poder te ajudar! Olha, não vou ficar bancando a babá... Da pra andar logo?"]
},







{
"key":"Lvl2_1",
"line":["On...Onde é que tou?","Ei! Você está bem? Você desmaiou!"," Desculpe deixa-lo aí, não podia trazê-lo adiante desacordado.","QUEM É VOCÊ?","Okay, okay. Vou te ajudar a pegar algo que vai te ajudar muito!","Já chega! Eu vou embora, cansei de você!","Eu sei porque você está aqui, Boris...","Como... Como é que você sabe meu nome???"," ","Ah cara... Que droga!"]
},

{
"key":"Lvl2_2",
"line":["Me divirto com isso sabia?","Do que cê tá falando?","Do parque, é claro! É engraçado.","O que tem de engraçado nisso tudo aqui?","A ironia. Você sabia que este parque nem chegou a ser usado pelos moradores de Pripyat?"," Estavam todos tão ansiosos."," É engraçado como algo tão inocente como este parque pode desviar a atenção de toda uma população da usina,"," tão mais impressionante, para algo insignificante.","Insignifi... Ah... O que você ia dizer antes? Algo... Pra me ajudar.","Aah! Agora quer minha ajuda! Muito bem, vê os carrinhos de bate-bate?","Embaixo do carrinho do meio tem uma manivela.","Eu achei que você fosse me ajudar!","E vai! A manivela não sairá de lá sozinha. Talvez tenha que empurrar alguns carrinhos."]
},

{
"key":"Lvl2_3",
"line":["Aqui está...","Ah só mais uma coisinha... Óleo, precisamos de óleo! Para a antiga loja de presentes!"]
},

{
"key":"Lvl2_4",
"line":["Ali, vê aquele caixote novinho? Pertencia a algum STALKER. Está trancado, eu sei, o óleo está aí dentro.","E como é que você espera que eu abra!"," ","Ora Boris..."]
},

{
"key":"Lvl2_5",
"line":["Não acredito... Era isso?"," ","Conseguiu? DAVAY ZDORAVA!! Okay, okay! Próximo passo, roda gigante!","Sempre tem mais um passo..."]
},

{
"key":"Lvl2_6",
"line":["Magnífica, não?","É... É sim...","Bem... Tem algo lá em cima que eu preciso que você pegue também. Aliás, este é você quem vai precisar.","Mas... Isso deve ter uns 25 metros de altura! Sem falar que não deve funcionar há uma década!","Exato! Pra isso precisaremos de uma manivela e óleo!","E o que é que tem lá em cima?","Quando pegar, você verá!","Pelo menos uma vez na vida, você pode ser menos misterioso e suspeito?","Posso. Mas não vou."]
},

{
"key":"Lvl2_7",
"line":["Um... UM URSINHO!!! TUDO ISSO POR UM URSINHO!!!","Tudo isso por seu ursinho, Boris... Lushka, o nome. Não lembra?","Como? Meu ursinho? Lushka...","Eu disse que te ajudaria, Boris... Venha, vamos pra usina. Última parada do nosso “tour”","Tem um atalho, um bueiro perto dos carrinhos de bate-bate. O túnel vai te levar até lá."]
},

{
"key":"Lvl2_8",
"line":["Mas que...","Que está esperando?","O que você está querendo dizer?","O bueiro! Porque ainda não entrou?","Eu... Eu não vou entrar ai dentro! Não vejo nada e meu medidor está louco!","Não está louco, está funcionando como deveria!","Anda logo! Mas tome cuidado com o tempo lá embaixo. Você só pode ficar exposto à radiação extrema alguns instantes."," Na segunda sala à direita há uma caixa com pílulas anti-radiação, elas te dão mais tempo conforme as consome.","É melhor você saber do que tá falando cara..."]
},








{
"key":"Lvl3_1",
"line":["Imagino que não tenha vindo até aqui pela usina.","Aliás, a foto também não estava onde procurou, não é mesmo?","Escuta... Você parece estar me ajudando, mas eu não sei nada de você...","Como tenho certeza que eu posso confiar em você!?","Oy! Não precisa se estressar, paciência!","Você está mais perto do que imagina...","Então por que não me ajuda a encontrar a droga da foto de volta nos apartamentos em Pripyat?","Já assistiu a filmagens de guerra? Segunda Guerra Mundial, Vietnã?","Mas... Ah tanto faz...","Ou talvez já tenha visto filmes dramáticos em que o protagonista sabia, de alguma forma,","que estava em perigo e carregava com si algo valioso.","Onde quer chegar?","Os vestiários da usina têm grandes armários de ferro. Um pra cada empregado.","Com a correria, ainda está quase tudo lá...","Me pergunto que tipo de coisas os funcionários guardavam trancadas ali.","Será que sabiam, de alguma forma, que estavam em perigo?","O que você acha que tem lá?","Com um pouco de sorte, o que você procura.","Não vai ser fácil chegar lá, no entanto...","Tem muitas portas trancadas e você não tem nenhuma chave, é melhor começar a procurar!","Por onde é que eu começo?","Você precisa de 3 chaves para chegar à sala de controle. Lá abrirá a 4ª porta.","Por que eu preciso ir na sala de controle para chegar no vestiário?","Porque... Preciso de... Um negócio.","Que negócio cara?","Eh… Eu te ajudei até agora, não foi? Confia em mim!"]
},

{
"key":"Lvl3_2",
"line":["Chave um, confere!"]
},


{
"key":"Lvl3_3",
"line":["Segunda chave no bolso!"]
},


{
"key":"Lvl3_4",
"line":["Terceira e última chave..."]
},


{
"key":"Lvl3_5",
"line":["Okay... O que eu aperto?","Eh… O grande botão vermelho!","Aquele que diz... Perigo?","Ahhh... tenho certeza que não é nada."]
},


{
"key":"Lvl3_6",
"line":["O quê… O que tá havendo aqui? Não é o vestiário!","Eu... Eu sei! Tem algo que precisamos aqui também...","Vejamos... Os armários! Abra!","Voz... O que tá havendo?","Por que não quer que eu chegue nos vestiários???","Eu.. Não é isso! Preciso mesmo da... Teia de aranha... Hmpf...","Tá bem... Eu só... Pensei se não seria melhor pra você não ver nada...","Voltar para casa, seguir sua vida normal como era...","Você tem um futuro brilhante! Não precisa disso... Tem uma nova família!","Eu... Eu sei... E vejo agora que só queriam me proteger..."," Na verdade... Eu acho que sempre soube desde que parti, só não queria admitir..."," ","A escolha é sua, Boris...","Os vestiários estão atrás da porta amarela...","O armário que procura é o vermelho, número 14.","Mas caso não queira, você sabe onde é a saída da usina..."]
}





]	
}



