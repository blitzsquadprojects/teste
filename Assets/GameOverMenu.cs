﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    public static bool IsDead = false;
    public GameObject GameOverMenuUI;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!PauseMenu.GameIsPaused)
        {
            if (Input.GetKeyDown(KeyCode.G))
            {
                if (!IsDead)
                {
                    Pause();
                }
            }
        }
    }

    public void Resume()
    {
        GameOverMenuUI.SetActive(false);
        Time.timeScale = 1f;
        IsDead = false;
    }

    void Pause()
    {
        GameOverMenuUI.SetActive(true);
        Time.timeScale = 0f;
        IsDead = true;
    }

    public void LoadOptions()
    {
        Debug.Log("Loading options...");
    }

    public void LoadMenu()
    {
        Debug.Log("Loading menu...");

        //Time.timeScale = 1f;
        //SceneManager.LoadScene("Menu");
    }

}
