﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtivarFinal : MonoBehaviour
{
    public GameObject portaAntiga;
    public GameObject portaNova;
    public GameObject final;
    public GameObject abrirArmario;
    public GameObject FecharPorta;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void PlayerEnter()
    {
        FecharPorta.SetActive(true);
        portaNova.SetActive(true);
        portaAntiga.SetActive(false);
        final.SetActive(true);
        abrirArmario.SetActive(true);
        gameObject.SetActive(false);
    }

    void PlayerExit()
    {
        CanvasController.instance.ChangeActionButton(null);
    }
}
